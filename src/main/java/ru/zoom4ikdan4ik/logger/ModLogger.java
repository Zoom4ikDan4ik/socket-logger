package ru.zoom4ikdan4ik.logger;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import ru.zoom4ikdan4ik.logger.sockets.ClientSocket;

import java.io.IOException;
import java.net.InetAddress;

@Mod(modid = "ModLogger", name = "ModLogger", acceptableRemoteVersions = "*")
public class ModLogger {
    private ClientSocket clientSocket;

    @Mod.Instance("ModLogger")
    public static ModLogger instance;

    public ClientSocket getClientSocket() {
        return this.clientSocket;
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) throws IOException {
        this.clientSocket = new ClientSocket(InetAddress.getByName("95.217.68.53"), 8005);
    }
}
